#! /usr/bin/python

# Script to list e-mail addresses of all members.
#
# How to use this script
# ======================
#
# You only need to pass the list of membership as argument. It will output
# the list of e-mail addresses on stdout. Thus, what you probably want to do
# is:
# $ ./list-addresses.py membership_new.txt > /tmp/addresses.txt

import re
import sys
import string

nospam_re = re.compile ("no_spam\.?")

def unmunge_email (addr):
    unmunged = nospam_re.sub ("", addr)
    return unmunged


comment_re = re.compile ("^ *#.*")
entry_re = re.compile (" *(.*?)\/(.*?)\/(.*?)\/(.*?) *")

filename = sys.argv[1]

handle = open (filename)

lines = handle.readlines ()

count = 0

for line in lines:
    line = comment_re.sub ("", line)
    string.strip (line)
    if line == "" or line == "\n":
        continue

    match = entry_re.search (line)
    if match:
        name = string.strip (match.group (1))
        email = string.lower (unmunge_email (string.strip (match.group (2))))
        contribution = string.strip (match.group (3))
        date = string.strip (match.group (4))
        count = count + 1
        print email

handle.close ()

