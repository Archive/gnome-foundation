#! /usr/bin/perl

# Script to send renewal notices to members who need to renew their membership.
#
# How to use this script
# ======================
#
# The first thing to do is to find all the memberships that will end
# before the end of the elections (i.e. before December 17th). It's easy:
# just search for something like "2002-" in membership_new.txt (people who
# were accepted before 2002-12-17 will have to renew).
#
# Copy all the lines for these members in a new file (for example
# need-to-renew.txt).
#
# Now you can use renew-mailout.pl. You may want to look at the code if you
# grok perl. This script opens a file and sends renewal notices to the members
# it finds in the file. Thus, you would call it like this:
# $ ./renew-mailout.pl --file=need-to-renew.txt
#
# Note that if you want to send a second (and last) renewal notice later,
# you can call it like this:
# $ ./renew-mailout.pl --reminder --file=need-to-renew.txt
#
# The script tells you how many mail it has sent.
#
# This script needs a MTA to send the renewal e-mails. If you don't have one or
# if you're not sure that your ISP allows you to directly send mails, it's
# probably better and safer to run the script from a gnome.org server.
#
# You may want to look at your mail server logs (and maybe keep them) to
# know if all mails were delivered. Last time I sent some renewals, there
# were 10 or 20 mail errors. In case of such errors, you can try to look for
# the new e-mail addresses of the members to send them another renewal.

use Getopt::Long;

sub print_help {
  print "Usage: renew_mailout.pl [--reminder] --file=s\n";
  print "\t--help      print this help\n";
  print "\t--reminder  send a reminder e-mail\n";
  print "\t--file=s    file containing the expirying memberships\n";
  print "\n";
  print "More help is available in the script header (just open it in a text editor).\n"
}

my $reminder = 0;

GetOptions (
    "reminder" => \$reminder,
    "file=s"   => \$filename,
    "help"     => \$help
    ) || die "Can't parse the command line.";

&print_help if ($help);
exit 0 if ($help);

if (!defined ($filename)) {
  print "Please specify a filename.\n";
  exit;
}

if (!(-e $filename and -f $filename)) {
  print "$filename doesn't exist.\n";
  exit;
}

$count = 0;
open (FILE, $filename);
while (<FILE>) {
	chomp;
	next if (m/^\#/);
	($name, $mail, $contrib, $date) = m/(.*)\/(.*\@.*)\/(.*)\/(.*)/os;
	next if ($mail eq "");
	$mail =~ s/no_spam\.//g;
	$count++;

	open MAIL, "| /usr/sbin/sendmail -t -i" or print "Error sending to $mail";
	print MAIL <<EOM;
To: $mail
From: GNOME Foundation Membership Committee <membership-committee\@gnome.org>
EOM
  if ($reminder) {
    print MAIL <<EOM;
Subject: GNOME Foundation Membership Renewal - Last Reminder

This mail is a reminder and there won't be any more reminder.
If you already have renewed your membership, then you can ignore this
mail.
Here is the previous mail you should have received:
EOM
  } else {
    print MAIL <<EOM;
Subject: GNOME Foundation Membership Renewal
EOM
  }
	print MAIL <<EOM;

Dear $name,

It is time to renew your GNOME Foundation Membership, which started (or
was last renewed) on $date and lasted two years.

When you first signed up, you stated your contribution to GNOME as
follow:

$contrib

If you want to renew your membership, please use the form at
http://foundation.gnome.org/membership/application.php.
Please remember to check the box to indicate you are an existing
member. Once your renewal is accepted, your membership will be
extended for two years. You shall receive an e-mail confirmation from
the committee.

If you don't want to renew your membership, please consider supporting
us through the Friends of GNOME program, details of which are available
at http://www.gnome.org/friends/

Thanks for your support and interest,

The GNOME Foundation Membership Committee

EOM

	close MAIL;
	print "Mailed $name <$mail>\n";
}

print "======================\n";
print "Mailed $count members.\n";
