#! /usr/bin/python

# Script to count the votes to get the results of a referendum.
#
# How to use this script
# ======================
#
# The script supposes there are two possible answers to the referendum: "yes"
# and "no".
#
# You should have the archives of the vote mailing list as an uncompressed mbox
# file. Let's suppose the file is vote-archives.mbox.
#
# You should know the secret string used to build the validation token for the
# current referendum. Let's suppose it's "secretstring".
#
# Let's also suppose that you made a list of voters in voters.txt (the format
# of this file should be the same as the one used in membership_new.txt).
#
# You should use this script like this:
# $ ./vote-counter-referendum.py vote-archives.mbox "secretstring" voters.txt
#
# Note that the script will count all occurence of "yes" and "no" in the vote.
# If none of those answers is found, then it will count the vote for the "none"
# answer.
#
# The script will output found errors before the results. You should try to
# solve these errors to have the real results. See doc/howto-elections.txt for
# help on this.

import re
import sys
import string
import md5

class Ballot:
    def __init__ (self):
        self.email = 0
        self.member = 0
        self.member_address = 0
        self.token = 0
        self.votes = []

    def add_vote (self, ans, id):
        self.votes.append ((ans, id))

class Answer:
    def __init__ (self, ans, id):
        self.ans = ans
        self.id = id
        self.count = 0
        self.voters = []
        
answers = {}

answer_tuples = [ \
    ("YES", 1), \
    ("NO", 2),
    ("none", 3)]    

for c in answer_tuples:
    answ = Answer (c[0], c[1])
    answers[answ.id] = answ

# these regexp are for the voters list
comment_re = re.compile ("^ *#.*")
entry_re = re.compile (" *(.*?)\/(.*?)no_spam.(.*?)\/.*?\/.*? *")

# these regexp are for the votes
from_line_re = re.compile ("^From: *(.*)")
member_re = re.compile (">? *Member: *([^ ].*) *")
member_address_re = re.compile (">? *Member Address: *([^ ]*)")
auth_token_re = re.compile (">? *Validation Token: *(.*)")
vote_re = re.compile (">? *(yes|no)", re.IGNORECASE)

ballots = []
current_ballot = 0

filename = sys.argv[1]      # mail archive file 
secret_cookie = sys.argv[2] # secret cookie
voter_list = sys.argv[3]    # list of valid voter addresses

# hash from valid addresses to whether they have sent in a ballot yet
valid_addresses = {}

voter_handle = open (voter_list)
for voter_line in voter_handle.readlines ():
    voter_line = comment_re.sub ("", voter_line)
    string.strip (voter_line)
    if voter_line == "" or voter_line == "\n":
        continue

    match = entry_re.search (voter_line)
    if match:
        name = string.strip (match.group (1))
        email = string.strip (match.group (2)) + string.strip (match.group (3))
        valid_addresses[name + email] = (name, email, 0)

voter_handle.close ()

handle = open (filename)
lines = handle.readlines ()
for line in lines:

    match = from_line_re.match (line)
    if match:
        email = string.strip (match.group (1))
        if current_ballot:
            ballots.append (current_ballot)
        current_ballot = Ballot ()
        current_ballot.email = email

        continue

    match = member_re.match (line)
    if match:
        member = string.strip (match.group (1))
        if (current_ballot.member and current_ballot.member != member):
            print "Different member ID in ballot from '%s' - '%s', '%s'" % (current_ballot.email, current_ballot.member, member)
        else:        
            current_ballot.member = member

        continue

    match = member_address_re.match (line)
    if match:
        member_address = string.strip (match.group (1))
        if (current_ballot.member_address and current_ballot.member_address != member_address):
            print "Different member address in ballot from '%s' - '%s', '%s'" % (current_ballot.email, current_ballot.member_address, member_address)
        else:        
            current_ballot.member_address = member_address

        continue
            
    match = auth_token_re.match (line)
    if match:
        token = string.strip (match.group (1))
        if (current_ballot.token and current_ballot.token != token):
            print "Different auth token in ballot from '%s' - '%s', '%s'" % (current_ballot.email, current_ballot.token, token)
        else:
            current_ballot.token = token

        continue

    match = vote_re.match (line)
    if match:
        ans = string.strip (match.group (1))
        id = 0
        for c in answer_tuples:
            if c[0] == ans:
                id = c[1]
        if id == 0:
            print "Unknown answer '%s' in ballot from %s <%s>" % (ans, current_ballot.member, current_ballot.member_address)
        else:
            current_ballot.add_vote (ans, id)

        continue
    
if current_ballot:
    ballots.append (current_ballot)    
        
handle.close ()

dup_tokens = {}
def md5_is_bad (b):
    key = b.member + " " + b.member_address + secret_cookie
    m = md5.new (key)
    token = m.hexdigest ()
    if token == b.token:
        if dup_tokens.has_key (token):
            print "Auth token occurs twice, someone voted more than once"
            return 0
        else:
            dup_tokens[token] = 1
        return 0
    else:
        print "Bad auth token is %s instead of %s for %s <%s> (key='%s')" % (token, b.token, b.member, b.member_address, key)
        return 1

def valid_voter (addr, name):
    return valid_addresses.has_key (name + addr)

valid_ballots = {}

i = 0
for b in ballots:
    error = 0
    if len (b.votes) == 0:
        current_ballot.add_vote ("none", 3)

    if not b.member:
        error = "missing member ID"
    elif not b.member_address:
        error = "missing member address"
    elif not b.token:
        error = "missing auth token"
    elif len (b.votes) != 1:
        error = "wrong number of answers (%d votes)" % len (b.votes)
    elif md5_is_bad (b):
        error = "bad authentication token"
    elif not valid_voter (b.member_address, b.member):
        error = "ballot from someone not on the list of valid voters"
    else:
        if valid_ballots.has_key (b.token):
            old = valid_ballots[b.token]
            print "Overriding previous valid ballot %d from %s with new ballot %d" % (old[1], old[0].member, i)
        valid_ballots[b.token] = (b, i)

    if error:
        print "Ignoring ballot %d from '%s' due to: %s" % (i, b.email, error)
        
    i = i + 1

def tupcmp (a, b):
    return cmp (a[1], b[1])

## Print results only after all errors have been printed, so
## we don't lose any errors.
valids = valid_ballots.values ()
valids.sort (tupcmp)
for (b, i) in valids:
    print "Ballot %d:" % i

    print "  From: " + b.email
    print "  Member: " + b.member
    print "  Member address: " + b.member_address
    print "  Token: " + b.token
    print "  Voted for: " + b.votes[0][0]

    id = b.votes[0][1]
    answers[id].count = answers[id].count + 1
    answers[id].voters.append (b.member)

    old = valid_addresses[b.member + b.member_address]
    valid_addresses[b.member + b.member_address] = (old[0], old[1], 1)

print "The following members did not vote:"
for key in valid_addresses.keys ():
    if not valid_addresses[key][2]:
        print "%s <%s>" % (valid_addresses[key][0], valid_addresses[key][1])

def cmpansw (a, b):
    return cmp (a.count, b.count)

answ_list = answers.values ()
answ_list.sort (cmpansw)

print ""
print ""
print "REFERENDUM RESULTS:"

print " %d of %d members cast a valid ballot" % (len (valids), len (valid_addresses.keys()))

for c in answ_list:
    print "  %s (%d votes)" % (c.ans, c.count)
