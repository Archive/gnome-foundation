#! /usr/bin/python
# -*- coding: UTF-8 -*-

# Script to count the votes to get the results of elections.
#
# How to use this script
# ======================
#
# You'll need to first update this script to have the right candidates: update
# the candidate_tuples variable. The numbers in this variable are the ID
# numbers that were assigned to each candidates in the ballot.
#
# You should have the archives of the vote mailing list as an uncompressed mbox
# file. Let's suppose the file is vote-archives.mbox.
#
# You should know the secret string used to build the validation token for the
# current referendum. Let's suppose it's "secretstring".
#
# Let's also suppose that you made a list of voters in voters.txt (the format
# of this file should be the same as the one used in membership_new.txt).
#
# You should use this script like this:
# $ ./vote-counter-elections vote-archives.mbox "secretstring" voters.txt
#
# The script will output found errors before the results. You should try to
# solve these errors to have the real results. See doc/howto-elections.txt for
# help on this.

import re
import sys
import string
import md5

class Ballot:
    def __init__ (self):
        self.email = 0
        self.member = 0
        self.member_address = 0
        self.token = 0
        self.votes = []

    def add_vote (self, name, id):
        for v in self.votes:
            v_id = v[1]
            if id == v[1]:
                print "Ignoring duplicate vote for candidate '%s' (ID# %d)" % (name, id)
                return
        self.votes.append ((name, id))

class Candidate:
    def __init__ (self, name, id):
        self.name = name
        self.id = id
        self.count = 0
	self.voters = []
        
candidates = {}

candidate_tuples = [ \
    ("Jonathan Blandford", 1), \
    ("Bryan Clark", 2), \
    ("Murray Cumming", 3), \
    ("Rodney Dawes", 4), \
    ("Miguel de Icaza", 5), \
    ("Christian Schaller", 6), \
    ("Jody Goldberg", 7), \
    ("Bill Haneman", 8), \
    ("Dom Lachowicz", 9), \
    ("Michael Meeks", 10), \
    ("Federico Mena-Quintero", 11), \
    ("David Neary", 12), \
    ("Tim Ney", 13), \
    ("Anne Østergaard", 14), \
    ("Germán Poó-Caamaño", 15), \
    ("Leslie Proctor", 16), \
    ("Owen Taylor", 17), \
    ("Daniel Veillard", 18), \
    ("Luis Villa", 19) ]    

for c in candidate_tuples:
    cand = Candidate (c[0], c[1])
    candidates[cand.id] = cand

# these regexp are for the voters list
comment_re = re.compile ("^ *#.*")
entry_re = re.compile (" *(.*?)\/(.*?)no_spam.(.*?)\/.*?\/.*? *")

# these regexp are for the votes
from_line_re = re.compile ("^From: *(.*)")
member_re = re.compile (">? *Member: *([^ ].*) *")
member_address_re = re.compile (">? *Member Address: *([^ ]*)")
auth_token_re = re.compile (">? *Validation Token: *(.*)")
vote_re = re.compile (">? *(.+) *\(ID# *([0-9]+)\)", re.U)

ballots = []
current_ballot = 0

filename = sys.argv[1]      # mail archive file 
secret_cookie = sys.argv[2] # secret cookie
voter_list = sys.argv[3]    # list of valid voter addresses

# hash from valid addresses to whether they have sent in a ballot yet
valid_addresses = {}

voter_handle = open (voter_list)
for voter_line in voter_handle.readlines ():
    voter_line = comment_re.sub ("", voter_line)
    string.strip (voter_line)
    if voter_line == "" or voter_line == "\n":
        continue

    match = entry_re.search (voter_line)
    if match:
        name = string.strip (match.group (1))
        email = string.strip (match.group (2)) + string.strip (match.group (3))
        valid_addresses[name + email] = (name, email, 0)

voter_handle.close ()

handle = open (filename)
lines = handle.readlines ()
for line in lines:

    match = from_line_re.match (line)
    if match:
        email = string.strip (match.group (1))
        if current_ballot:
            ballots.append (current_ballot)
        current_ballot = Ballot ()
        current_ballot.email = email

        continue

    match = member_re.match (line)
    if match:
        member = string.strip (match.group (1))
        if (current_ballot.member and current_ballot.member != member):
            print "Different member ID in ballot from '%s' - '%s', '%s'" % (current_ballot.email, current_ballot.member, member)
        else:        
            current_ballot.member = member

        continue

    match = member_address_re.match (line)
    if match:
        member_address = string.strip (match.group (1))
        if (current_ballot.member_address and current_ballot.member_address != member_address):
            print "Different member address in ballot from '%s' - '%s', '%s'" % (current_ballot.email, current_ballot.member_address, member_address)
        else:        
            current_ballot.member_address = member_address

        continue
            
    match = auth_token_re.match (line)
    if match:
        token = string.strip (match.group (1))
        if (current_ballot.token and current_ballot.token != token):
            print "Different auth token in ballot from '%s' - '%s', '%s'" % (current_ballot.email, current_ballot.token, token)
        else:
            current_ballot.token = token

        continue

    match = vote_re.match (line)
    if match:
        name = string.strip (match.group (1))
        id = string.strip (match.group (2))

        id = int(id)

        if not candidates.has_key (id):
            print "Unknown candidate '%s' (ID# %d) in ballot from %s <%s>" % (name, id, current_ballot.member, current_ballot.member_address)
        elif not candidates[id].name == name:
            print "Candidate name '%s' for ID# %d doesn't match, expected '%s'" % (name, id, candidates[id].name)    
        else:
            current_ballot.add_vote (name, id)

        continue
    
if current_ballot:
    ballots.append (current_ballot)    
        
handle.close ()

dup_tokens = {}
def md5_is_bad (b):
    key = b.member + " " + b.member_address + secret_cookie
    m = md5.new (key)
    token = m.hexdigest ()
    if token == b.token:
        if dup_tokens.has_key (token):
            print "Auth token occurs twice, someone voted more than once"
            return 0
        else:
            dup_tokens[token] = 1
        return 0
    else:
        print "Bad auth: token should be %s instead of %s for %s <%s> (key='%s')" % (token, b.token, b.member, b.member_address, key)
        return 1

def valid_voter (addr, name):
    return valid_addresses.has_key (name + addr)

valid_ballots = {}

i = 0
for b in ballots:
    error = 0
    if not b.member:
        error = "missing member ID"
    elif not b.member_address:
        error = "missing member address"
    elif not b.token:
        error = "missing auth token"
    elif len (b.votes) > 11:
        error = "too many votes (%d votes)" % len (b.votes)
    elif md5_is_bad (b):
        error = "bad authentication token"
    elif not valid_voter (b.member_address, b.member):
        error = "ballot from someone not on the list of valid voters"
    else:
        if valid_ballots.has_key (b.token):
            old = valid_ballots[b.token]
            print "Overriding previous valid ballot %d from %s with new ballot %d" % (old[1], old[0].member, i)
        valid_ballots[b.token] = (b, i)

    if error:
        print "Ignoring ballot %d from '%s' due to: %s" % (i, b.email, error)
        
    i = i + 1

def tupcmp (a, b):
    return cmp (a[1], b[1])

## Print results only after all errors have been printed, so
## we don't lose any errors.
valids = valid_ballots.values ()
valids.sort (tupcmp)
for (b, i) in valids:
    print "Ballot %d:" % i

    print "  From: " + b.email
    print "  Member: " + b.member
    print "  Member address: " + b.member_address
    print "  Token: " + b.token
    print "  Voted for %d candidates: " % len (b.votes)

    voted_for = []

    old = valid_addresses[b.member + b.member_address]
    valid_addresses[b.member + b.member_address] = (old[0], old[1], 1)

    for v in b.votes:
        id = v[1]
        candidates[id].count = candidates[id].count + 1
        candidates[id].voters.append (b.member)
        voted_for.append (candidates[id].name)

    for v in voted_for:
        print "   " + v

print "The following members did not vote:"
for key in valid_addresses.keys ():
    if not valid_addresses[key][2]:
        print "%s <%s>" % (valid_addresses[key][0], valid_addresses[key][1])

def cmpcand (a, b):
    return cmp (a.count, b.count)

cand_list = candidates.values ()
cand_list.sort (cmpcand)

print ""
print ""
print "ELECTION RESULTS:"

print " %d of %d members cast a valid ballot" % (len (valids), len (valid_addresses.keys()))

for c in cand_list:
    print "  %s (%d votes)" % (c.name, c.count)
