#!/usr/bin/perl

# Script to send a ballot to a voter using another e-mail address than the
# registered one for the voter.
#
# How to use this script
# ======================
#
# You probably want to first update the subject of the e-mail that will be
# sent. Look for "Subject:" in this file and update the subject.
#
# You should know the secret string used to build the validation token for the
# current elections/referendum. Let's suppose it's "secretstring".
#
# Let's also suppose the ballot is in ballot.txt.
#
# If the member Foo Bar has foo@bar.com as registered e-mail address but is
# asking you to send the ballot to newaddress@foobar.com, then you should use
# this script like this:
# $ ./resend-ballot.pl ballot.txt newaddress@foobar.com "Foo Bar" foo@bar.com "secretstring"
#
# This script needs a MTA to send the e-mail. If you don't have one or if
# you're not sure that your ISP allows you to directly send mails, it's
# probably better and safer to run the script from a gnome.org server.
#
# You may want to look at your mail server logs (and maybe keep them) to
# know if the mail was delivered.

use Digest::MD5 qw (md5_hex);
use MD5;
use Mail::Internet;

die "Usage: resend-ballot.pl <ballot template> <mail to> <member name> <member addr> <secret string>" unless $#ARGV == 4;

my $SECRET_STRING = $ARGV[4];

open BALLOT, "<$ARGV[0]" || die "Cannot open ballot file $ARGV: $!";
my @ballot = <BALLOT>;
close BALLOT;

for (my $i = 0; $i <= $#ballot; $i++) {
    push @dear_indexes,     $i if $ballot[$i] =~ /^\s*Dear <member>/;
    push @identity_indexes, $i if $ballot[$i] =~ /^\s*Member:/;
    push @addr_indexes,     $i if $ballot[$i] =~ /^\s*Member Address:/;
    push @token_indexes,    $i if $ballot[$i] =~ /^\s*Validation Token:/;
}

my $head = Mail::Header->new ( [ "From: GNOME Foundation Elections <elections\@gnome.org>",
                                 "Subject: Official Ballot for GNOME Foundation Elections",
                                 "Reply-To: vote\@gnome.org" ]);

my $dest = $ARGV[1];
my $name = $ARGV[2];
my $addr = $ARGV[3];

my $hash = MD5->hexhash ("$identity $addr" . $SECRET_STRING);

foreach $index (@dear_indexes) {
    $ballot[$index] = "Dear $identity,\n";
}

foreach $index (@identity_indexes) {
    $ballot[$index] = "Member: $identity\n";
}

foreach $index (@addr_indexes) {
    $ballot[$index] = "Member Address: $addr\n";
}

foreach $index (@token_indexes) {
    $ballot[$index] = "Validation Token: $hash\n";
}

$head->replace ("To", $dest);
my $mail = Mail::Internet->new (Header => $head, Body => \@ballot);
unless ($mail->smtpsend ()) {
    print "Error: Could not send to $identity using address $dest!\n";
} else {
    print "Success.\n";
}
