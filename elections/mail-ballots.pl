#!/usr/bin/perl

# Script to send ballots to all voters.
#
# How to use this script
# ======================
#
# You probably want to first update the subject of the e-mail that will be
# sent. Look for "Subject:" in this file and update the subject.
#
# You should know the secret string used to build the validation token for the
# current elections/referendum. Let's suppose it's "secretstring".
#
# Let's also suppose that the ballot is in ballot.txt and that you made a list
# of voters in voters.txt (the format of this file should be the same as the
# one used in membership_new.txt).
#
# You should use this script like this:
# $ ./mail-ballots.pl voters.txt ballot.txt "secretstring"
#
# This script needs a MTA to send the e-mails. If you don't have one or if
# you're not sure that your ISP allows you to directly send mails, it's
# probably better and safer to run the script from a gnome.org server.
#
# You may want to look at your mail server logs (and maybe keep them) to
# know if the mail was delivered. There are usually 10-15 errors. In case of
# such errors, you can try to look for the new e-mail addresses of the voters
# to ask them if they want to update their registered e-mail address and
# receive their ballot.

use Digest::MD5 qw (md5_hex);
use MD5;
use Mail::Internet;

die "Usage: mail-ballots.pl <recipient list> <ballot template> <secret string>\n" unless $#ARGV == 2;

my $SECRET_STRING = $ARGV[2];

open BALLOT, "<$ARGV[1]" || die "Cannot open ballot file $ARGV: $!";
my @ballot = <BALLOT>;
close BALLOT;

for (my $i = 0; $i <= $#ballot; $i++) {
    push @dear_indexes,     $i if $ballot[$i] =~ /^\s*Dear <member>/;
    push @identity_indexes, $i if $ballot[$i] =~ /^\s*Member:/;
    push @addr_indexes,     $i if $ballot[$i] =~ /^\s*Member Address:/;
    push @token_indexes,    $i if $ballot[$i] =~ /^\s*Validation Token:/;
}

my $head = Mail::Header->new ( [ "From: GNOME Foundation Elections <elections\@gnome.org>",
                                 "Subject: Official Ballot for 2004 GNOME Foundation Elections",
                                 "Reply-To: vote\@gnome.org" ]);

open RECIPS, "<$ARGV[0]" || die "Cannot open file $ARGV: $!";

my $sent = 0;
my $errors = 0;

while (<RECIPS>) {
    chomp; 
    next if (/^\#/ || /^$/);

    if (!(/^ *(.*)\/(.*)\@no_spam\.(.*)\/.*\/.* *$/)) {
        print "Error for line: $_\n";
        next;
    }
    my $identity = $1;
    my $addr = "$2\@$3";
    my $hash = MD5->hexhash ("$identity $addr", $SECRET_STRING);

    foreach $index (@dear_indexes) {
        $ballot[$index] = "Dear $identity,\n";
    }

    foreach $index (@identity_indexes) {
        $ballot[$index] = "Member: $identity\n";
    }

    foreach $index (@addr_indexes) {
        $ballot[$index] = "Member Address: $addr\n";
    }

    foreach $index (@token_indexes) {
        $ballot[$index] = "Validation Token: $hash\n";
    }

    $head->replace ("To", $addr);
    my $mail = Mail::Internet->new (Header => $head, Body => \@ballot);
    unless ($mail->smtpsend ()) {
        print "Error: Could not send to $addr ($identity)!\n";
        $errors++;
    } else {
        $sent++;
    }
}

close RECIPS;

print "Mailed $sent ballots; $errors could not be mailed.\n";
