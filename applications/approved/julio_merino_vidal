Contact Information:
--------------------
Name: Julio M. Merino Vidal
E-mail: jmmv AT menta.net
irc.gnome.org nickname (if any): 
cvs.gnome.org username (if any): 

Previous GNOME Foundation member: no

GNOME contributions:
--------------------

Summary:
NetBSD portability fixes, pkgsrc packaging, miscellaneous bug fixes.

Detailed description:
I am the maintainer of most of the GNOME packages in the NetBSD's Packages collection, known as pkgsrc.  pkgsrc runs on many operating systems, so this is why I said 'pkgsrc packaging' instead of 'NetBSD packaging'.

Being the maintainer of almost all GNOME packages,   I did the whole update for all versions of the 2.6 branch, and I'm working on the 2.8 packages at this moment.  I also take responsibility for bug reports filed against these packages (not that it has anything to do directly with GNOME).

As regards to portability to NetBSD, GNOME had multiple (not to say many) issues that made it unusable; I started the porting process when 2.0 was published, but it wasn't until 2.4 that we got a semi-usable desktop.  Now, with 2.8, almost everything works as expected.  (My final goal is to get it working "out of the box", without having to keep external patches outside the packaging system.  However, don't consider the "final goal" as something that means that I'll stop working; in fact, this job will never end as long as new versions of GNOME appear, and as long as I have enough free time to work on this.)

These portability bug fixes ranged from build failures (due to the use of missing futures) to more complex things like incorrect runtime behavior, often caused by problems in the use of threads or shared libraries.  Maybe the bugs that I enjoyed most debugging and patching were ones found in the glib2 library (140329 and 139567): plugins were not loaded correctly at runtime by the dlopen abstraction layer, so many programs using these features failed (most notably gstreamer and evolution).

Aside from portability bug fixes, I've also reported bugs regarding to general behavior (not related to NetBSD).  Moreover, I've also done some funcionality improvements like the addition of the '--makefile-uninstall-rule' (104487) and '--unload' (130129) flags found in GConf2, or some cleanups in configuration scripts.

Almost all of my bug reports include a patch.  And nearly all of them have been already applied to the CVS HEAD of the project affected.

Contacts:
I don't think I can list anybody in particular here.  My bugfixes were filed against lots of different projects, so it will be difficult that a project maintainer confirms that I've done big contributions to them.  Maybe the most remarkable could be the maintainers of GConf2 (Havoc Pennington, hp redhat com), gstreamer (David Schleef, ds schleef org) and somebody from glib/gtk (like Matthias Clasen, mclasen redhat com).

However, the best way to check what I have done is to look at the bugs I filed, their status and the attached patches.  A simple query that could match them could be the following:

http://bugzilla.gnome.org/buglist.cgi?short_desc_type=allwordssubstr&short_desc=&long_desc_type=allwordssubstr&long_desc=&status_whiteboard_type=allwordssubstr&status_whiteboard=&keywords_type=anywords&keywords=&emailreporter1=1&emailtype1=regexp&email1=jmmv%40%28hispabsd.org%7Cmenta.net%29&emailtype2=substring&email2=&bugidtype=include&bug_id=&changedin=&chfieldfrom=&chfieldto=Now&chfieldvalue=&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0=

which actually lists 133 reports.

Furthermore, to verify that I really am the maintainer of most of the packages found in pkgsrc, you can check NetBSD's CVS repository.

For the CVS logs, go to the following address:

http://cvsweb.netbsd.org/bsdweb.cgi/pkgsrc/

and navigate to directories like meta-pkgs/gnome, devel/glib2, devel/atk, devel/pango, devel/libbonobo, devel/libgnome, devel/GConf2, sysutils/gnome-vfs2, multimedia/gstreamer, multimedia/gst-plugins, x11/gtk2, x11/gnome-panel, x11/gnome-session... and a very, very large etcetera.  When you are in those directories, just open the Makefile and check the value in the MAINTAINER field to verify it's me.  For the history, just open the logs of these files and see my activity.

All of these commits were also logged in the cvs changes mailing list; this means that you can check them from there too (which may be easier).  Going to:

http://mail-index.netbsd.org/pkgsrc-changes/2004/04/

will show you the whole update to GNOME 2.6.  To check for other updates, you only have to select the appropiate year and month from the following page:

http://mail-index.netbsd.org/pkgsrc-changes/

(Note that the update to 2.8 will not happen under the "pkgsrc freeze" (a development freeze, just like you do in GNOME) is over, which won't be until the next monday.  So don't get confused if you still don't see it there ;-)

Other comments:
---------------
I would like to be part of the foundation mainly to be able to vote and to "represent" GNOME; I mean, being a member will make me feel more part of the project ;-)

[Application received at Thu Sep 16 9:51:02 2004 (Eastern time)]

Application accepted by Gael CHAMOULAUD <strider@gnome.org> 2004-09-26
