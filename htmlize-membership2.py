#! /usr/bin/python

# Script to build an HTML page from the list of members. It's used to
# automatically build http://foundation.gnome.org/membership/members.html
#
# How to use this script
# ======================
#
# You only need to pass the list of membership as argument. It will output
# the page on stdout. Thus, what you probably want to do is:
# $ ./htmlize-membership2.py membership_new.txt > /tmp/members.html

import re
import sys
import string

print '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">'
print '<html><link rel="stylesheet" type="text/css" href="http://www.gnome.org/default.css"><link rel="stylesheet" type="text/css" href="http://foundation.gnome.org/foundation.css"><link rel="icon" type="image/png" href="http://www.gnome.org/img/logo/foot-16.png">'
print ''
print '  <head>'
print '    <title>GNOME Foundation Members</title>'
print '    <meta name="cvsdate" content="$Date$">'
print '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>'
print '  </head>'
print ''
print '  <body><div id="body">'
print ''
print '    <h1>GNOME Foundation Membership List</h1>'
print ''
print '    <p>'
print '      Send comments, questions, and updates to'
print '      <a href="mailto:membership-committee@gnome.org">membership-committee@gnome.org</a>.'
print '    </p>'
print '    <p>'
print '      Current members:'
print '    </p>'
print ''
print '    <ul>'

def munge_email (addr):
    munged = ""
    for c in addr:
        if c == '@':
            munged = munged + '&#64;'
        elif c == '.':
            munged = munged + '&#46;'
        else:
            munged = munged + c
    return munged

comment_re = re.compile ("^ *#.*")
entry_re = re.compile (" *(.*?)\/(.*?)\/(.*?)\/(.*?) *")

filename = sys.argv[1]

handle = open (filename)

lines = handle.readlines ()

count = 0

for line in lines:
    line = comment_re.sub ("", line)
    string.strip (line)
    if line == "" or line == "\n":
        continue

    match = entry_re.search (line)
    if match:
        name = string.strip (match.group (1))
        email = munge_email (string.strip (match.group (2)))
        contribution = string.strip (match.group (3))
        date = string.strip (match.group (4))
        count = count + 1
	print '      <li>%s &lt;%s&gt; (%s)</li>' % (name, email, contribution)

handle.close ()

print '    </ul>'
print ''
print '    <p>'
print '      %s members' % (count)
print '    </p>'
print ''
print '</div>'
print ''
print '<div id="sidebar">'
print '        <p class="section">Foundation</p>'
print '        <ul>'
print '                <li><a href="http://foundation.gnome.org/about/">About the Foundation</a></li>'
print '                <li><a href="http://foundation.gnome.org/membership/">Membership</a></li>'
print '                <li><a href="http://foundation.gnome.org/elections/">Elections</a></li>'
print '                <li><a href="http://foundation.gnome.org/referenda/">Referenda</a></li>'
print '                <li><a href="http://foundation.gnome.org/legal/">Legal</a></li>'
print '                <li><a href="http://foundation.gnome.org/finance/">Finance</a></li>'
print '                <li><a href="http://foundation.gnome.org/contact/">Contact</a></li>'
print '        </ul>'
print '        <ul>'
print '                <li><a href="http://www.gnome.org/press/">Press</a></li>'
print '                <li><a href="http://www.gnome.org/friends/">Donate to GNOME</a></li>'
print '        </ul>'
print ''
print '</div>'
print ''
print '<div id="hdr">'
print '        <div id="logo"><a href="http://foundation.gnome.org/"><img src="http://www.gnome.org/img/spacer" alt="Home" /></a></div>'
print '        <div id="banner"><img src="http://www.gnome.org/img/spacer" alt="" /></div>'
print '        <p class="none"></p>'
print '        <div id="hdrNav">'
print '                <a href="http://www.gnome.org/about/">About GNOME</a> &middot;'
print '                <a href="http://www.gnome.org/start/2.4/">Download</a> &middot;'
print '                <!--<a href="http://www.gnome.org/contribute/"><i>Get Involved!</i></a> &middot;-->'
print '                <a href="http://www.gnome.org/">Users</a> &middot;'
print '                <a href="http://developer.gnome.org/">Developers</a> &middot;'
print '                <a href="http://foundation.gnome.org/"><b>Foundation</b></a> &middot;'
print '                <a href="http://www.gnome.org/contact/">Contact</a>'
print '        </div>'
print '</div>'
print ''
print '<div id="copyright">'
print 'Copyright &copy; 2003, <a href="http://www.gnome.org/">The GNOME Project</a>.<br />'
print 'Maintained by the <a href="mailto:membership-committee@gnome.org">GNOME Foundation Membership and Elections Committee</a>.<br />'
print '<a href="http://validator.w3.org/check/referer">Optimised</a> for'
print '<a href="http://www.w3.org/">standards</a>.'
print 'Hosted by <a href="http://www.redhat.com/">Red Hat</a>.'
print '</div>'
print ''
print '</body>'
print ''
print '</html>'
